import org.junit.Test;
import static org.junit.Assert.*;

public class GradeCalculatorTest {
	
	@SuppressWarnings("deprecation")
	@Test
	public void testMethodCoverageCalculationPass()
	{
		GradeCalculator g=new GradeCalculator();
		String regexInt ="[0-9]+";
		int assignmentId=5001;
		int courseId=500;
		int userName=101;
		double methodCoverage=11.0;
		assertNotEquals(userName,"");
		assertTrue(Integer.toString(userName).matches(regexInt));
		assertTrue(Integer.toString(assignmentId).matches(regexInt));
		assertTrue(Integer.toString(courseId).matches(regexInt));
		assertEquals("Method coverage is percentage and should return 1<= x <= 100", 11.0, g.calculateMethodCoverage(courseId, assignmentId, userName),methodCoverage);
	}
	
	@Test
	public void testMethodCoverageCalculationFail()
	{
		GradeCalculator g=new GradeCalculator();
		String regexInt ="[0-9]+";
		int assignmentId=5001;
		int courseId=500;
		int userName=101;
		double methodCoverage=101.0;
		assertNotEquals(userName,"");
		assertTrue(Integer.toString(userName).matches(regexInt));
		assertTrue(Integer.toString(assignmentId).matches(regexInt));
		assertTrue(Integer.toString(courseId).matches(regexInt));
		assertNotEquals("Method coverage is percentage and should return 1<= x <= 100", 11.0, g.calculateMethodCoverage(courseId, assignmentId, userName),methodCoverage);
	}
	
	@Test
	public void testProgramCoverageCalculationPass()
	{
		GradeCalculator g=new GradeCalculator();
		String regexInt ="[0-9]+";
		int assignmentId=5001;
		int courseId=500;
		int userName=101;
		double codeCoverage=11.0;
		assertNotEquals(userName,"");
		assertTrue(Integer.toString(userName).matches(regexInt));
		assertTrue(Integer.toString(assignmentId).matches(regexInt));
		assertTrue(Integer.toString(courseId).matches(regexInt));
		assertEquals("Code coverage is percentage and should return 1<= x <= 100", 11.0, g.calculateProgramCoverage(courseId, assignmentId, userName),codeCoverage);
	}
	
	@Test
	public void testProgramCoverageCalculationFail()
	{
		GradeCalculator g=new GradeCalculator();
		String regexInt ="[0-9]+";
		int assignmentId=5001;
		int courseId=500;
		int userName=101;
		double codeCoverage=101.0;
		assertNotEquals(userName,"");
		assertTrue(Integer.toString(userName).matches(regexInt));
		assertTrue(Integer.toString(assignmentId).matches(regexInt));
		assertTrue(Integer.toString(courseId).matches(regexInt));
		assertNotEquals("Code coverage is percentage and should return 1<= x <= 100", 11.0, g.calculateProgramCoverage(courseId, assignmentId, userName),codeCoverage);
	}
	
	@Test
	public void testGradeCalculationPass()
	{
		GradeCalculator g=new GradeCalculator();
		int courseId=500;
		int assignmentId=5001;
		int userName[]={101,105,110,102};
		double codeCoverage[]={11.00,12.00,13.00,14.00};
		assertNotEquals(userName,"");
		for(int i=0;i<userName.length;i++)
		{
			assertEquals("Code coverage should be calculated for user who has uploaded assignment", 11.0, g.calculateProgramCoverage(courseId, assignmentId, userName[i]),codeCoverage[i]);
		}
		
		
	}
}
