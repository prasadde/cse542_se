import org.junit.Test;
import static org.junit.Assert.*;

import org.junit.Before;

public class UserTest {
	
	@Before
	public void dummyValidUser()
	{
		User u=new User();
		u.userName=101;
		u.password="abcdef";
		u.email="abc@buffalo.edu";
		u.firstName="lmn";
		u.lastName="xyz";
		addUser(u); 
	}
	
	public void addUser(User u)
	{
		//Adds User U to database
	}
	//Testing Good Data
	@Test
	public void testValidUserInput()
	{   
		int userName=101;
		String password="abcdef";
		assertTrue("UserName is int and UserPassword is String",User.login(userName,password));
		
	}
	
	//Testing Bad Data
	@Test
	public void testInvalidUserInput()
	{   
		int userName=101;
		String userPassword="abcdef((";
		assertTrue("UserName must be int and UserPassword must be Alphanumeric",User.login(userName,userPassword));
	}
	
	@Test
	public void testNullUserInput()
	{
		int userName=101;
		String userPassword="";
		assertFalse("UserName or UserPassword Should not be null/empty",User.login(userName,userPassword));
		
	}

}
