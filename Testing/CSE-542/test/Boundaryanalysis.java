import org.junit.Test;
import static org.junit.Assert.*;

public class BoundaryAnalysisTest {
@Test
    public void BounadaryValid1()
    {
        User m=new User();
        String firstName= "Sean";
        String lastName= "Sanders";
        String userEmail = "seansanders@buffalo.edu";
        int userName = 1;
        String password =  "Cse542";
        assertTrue("Valid boundary condition fails", m.registerNewUser(userName, password, userEmail, firstName, lastName));
    }
@Test
    public void BounadaryInValid1()
    {
        User m=new User();
        String firstName= "Phani";
        String lastName= "Ram";
        String userEmail = "phanir@buffalo.edu";
        int userName = 2;
        String password =  "Cs542";
        assertFalse("User registered with invalid password length", m.registerNewUser(userName, password, userEmail, firstName, lastName));
    }

@Test
public void BounadaryValid2()
    {
        User m=new User();
        String firstName= "Sid";
        String lastName= "Shah";
        String userEmail = "sidshah@buffalo.edu";
        int userName = 3;
        String password = "projectCse12";
        assertTrue("Valid boundary condition fails", m.registerNewUser(userName, password, userEmail, firstName, lastName));
    }

@Test
    public void BounadaryInValid2()
    {
        User m=new User();
        String firstName= "Nikhil";
        String lastName= "Selvaraj";
        String userEmail = "nikhils@buffalo.edu";
        int userName = 4;
        String password = "projectCse542";
        assertFalse("User registered with invalid password length", m.registerNewUser(userName, password, userEmail, firstName, lastName));
    }
}