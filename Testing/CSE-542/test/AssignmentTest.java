import org.junit.Test;
import static org.junit.Assert.*;

public class AssignmentTest {
	@Before
	public void dummyValidAssignment()
	{
		Assignment a=new Assignment();
		a.assignmentId=20;
		a.courseId=542;
		addDummyAssignment(a);
	}
	public void addDummyAssignment(Assignment a)
	{
		//Adds Assignment to a database
	}
	@Test
   public void testAddAssignmentFail()
   {
	   int assignmentId=20;
	   int courseId=950;
	   assertFalse("Course does not exist..cannot add assignment",Assignment.addAssignment(courseId,assignmentId));
   }
   @Test
   public void testAddAssignmentPass()
   {
	   int assignmentId=20;
	   int courseId=542;
	   assertTrue("Course does not exist..cannot add assignment",Assignment.addAssignment(courseId,assignmentId));
   }
   @Test
   public void testDeleteAssignmentFail()
   {
	   int assignmentId=20;
	   int courseId=900;
	   assertFalse("Course does not exist..cannot delete assignment",Assignment.deleteAssignment(courseId,assignmentId));
   }
   @Test
   public void testDeleteAssignmentPass()
   {
	   int assignmentId=20;
	   int courseId=542;
	   assertTrue("Course does not exist..cannot delete assignment",Assignment.deleteAssignment(courseId,assignmentId));
   }
}
