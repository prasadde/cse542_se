import org.junit.Test;
import static org.junit.Assert.*;
import static org.junit.Assert.*;
public class CourseTest {
	@Before
	public void dummyValidCourse()
	{
		Course c=new Course();
		c.courseId=542;
		c.courseDescription="SEC";
		addDummyCourse(c);
	}
	public void addDummyCourse(Course c)
	{
		//Adds Course to a database
	}
	@Test
   public void testDeleteCoursePass()
   {
	   int courseId=542;
	   int assignmentList[]={100,201,300,408,503,608,500,290};
	   if(Course.deleteCourse(courseId))
	   {
		   for(int i=0;i<assignmentList.length();i++)
		      assertFalse("Assignment associated with course not deleted",Assignment.deleteAssignment(courseId,assignmentList[i]));
	   }
   }
	public void testDeleteCourseFail()
	   {
		   int courseId=542;
		   int assignmentList[]={100,201,300,408,503,608,500,290};
		   if(Course.deleteCourse(courseId))
		   {
			   for(int i=0;i<assignmentList.length();i++)
			      assertTrue("Assignment associated with course not deleted",Assignment.deleteAssignment(courseId,assignmentList[i]));
		   }
	   }
}
