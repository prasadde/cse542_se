
public class GradeCalculator {
	
	private int assignmentId;
	private int courseId;
	private double methodCoverage;
	private double codeCoverage;
	
	public double calculateMethodCoverage(int courseId,int assignmentId, int userName)
	{
		//Calculate method coverage for file uploaded by userName for selected assignmentId
		return methodCoverage;
	}
	
	public double calculateProgramCoverage(int courseId,int assignmentId, int userName)
	{
		//Calculate code coverage for file uploaded by userName for selected assignmentId
		return codeCoverage;
	}
	
}
